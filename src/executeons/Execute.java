/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package executeons;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import static executeons.ExecuteONS.trace;

/**
 *
 * @author lucasrc
 */
public class Execute implements Runnable {

    private String raClass;
    private int seed;
    private double load;

    public Execute(String raClass, int seed, double load) {
        this.raClass = raClass;
        this.seed = seed;
        this.load = load;
    }

    @Override
    public void run() {
        try {
            Runtime r = Runtime.getRuntime();
            Process process;
            BufferedReader br;
            String saida;
            File file;
            
            String command = "java -jar " + ExecuteONS.jarFile + " -f " + ExecuteONS.simConfigFile + " -s " + seed + " -ra " + raClass + " -l "
                    + load + " -json";
            if (trace) {
                command += " -trace";
            }
            process = r.exec(command);

            file = new File(ExecuteONS.dir + "/" + raClass + "_seeds" + "/" + raClass + "_seed_" + seed + "_" + load);
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);

            br = new BufferedReader(new InputStreamReader(process.getInputStream()));
            saida = null;
            while ((saida = br.readLine()) != null) {
                bw.write(saida);
                bw.newLine();
            }
            br.close();
            bw.close();
            fw.close();
            
            //process = r.exec(new String[]{"sh", "-c", "mv "+ ExecuteONS.dir + "/"+ raClass +"_seed* "+ ExecuteONS.dir + "/"+raClass+"_seeds"});
            System.out.println("\nEnd of execution - Class: "+raClass+ " Seed: " + seed + " Load: " + load);

        } catch (IOException iOException) {
            iOException.printStackTrace();
        }
    }
}
