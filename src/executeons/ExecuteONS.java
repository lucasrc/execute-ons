/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package executeons;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lucasrc
 */
public class ExecuteONS {

    protected static String simConfigFile = "", jarFile = "", dir = "", toEmail = "";
    protected static String[] raClasses;
    protected static int seed[], thread;
    protected static double minload, maxload, step = 1.0;
    protected static boolean trace = false;
    
    public static void main(String[] args) {
        ArgParsing.parse(args);
        Date currentTime = new Date();
        String date = new SimpleDateFormat("dd/MM/yyyy").format(currentTime);
        String hour = new SimpleDateFormat("HH:mm:ss").format(currentTime);
        createFolder();
        Runtime r = Runtime.getRuntime();
        Process process;
        
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        ThreadPoolExecutor executorPool = new ThreadPoolExecutor(thread, thread, 3, TimeUnit.DAYS, new LinkedBlockingQueue<Runnable>());
        
        
        for (int i = 0; i < raClasses.length; i++) {    
            try {
                process = r.exec("mkdir " + ExecuteONS.dir + "/" + raClasses[i] + "_seeds -p");
                for (int j = 0; j < seed.length; j++) {
                    for (double load = minload; load <= maxload; load += step) {
                        Execute exec = new Execute(raClasses[i], seed[j], load);
                        executorPool.execute(exec);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(ExecuteONS.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        executorPool.shutdown();
        while(!executorPool.isTerminated());
        currentTime = new Date();
        String date2 = new SimpleDateFormat("dd/MM/yyyy").format(currentTime);
        String hour2 = new SimpleDateFormat("HH:mm:ss").format(currentTime);
        JavaMailApp email = new JavaMailApp();
        if (!toEmail.isEmpty()) {
            String body = "";
            body += "Onsrun\n\nSimulation started on: " + date + " " + hour + "\n\nRAClass:\n";
            for (int i = 0; i < raClasses.length; i++) {
                body += "\t" + raClasses[i] + "\n";
            }
            body += "Seeds: ";
            for (int i = 0; i < seed.length; i++) {
                body += " " + seed[i];
            }
            body += "\nLoads: Min: " + minload + ", Max: " + maxload + ", Step: " + step + "\n";
            body += "\nSimulation finished in: " + date2 + " " + hour2 + "\n";
            email.sendEmail(toEmail, body);
        }
    }

    private static void createFolder() {
        Runtime r = Runtime.getRuntime();
        try {
            r.exec("rm -rf " + dir);
            r.exec("mkdir " + dir);
        } catch (IOException ex) {
            Logger.getLogger(ExecuteONS.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static boolean isAlive(Thread[] t) {
        for (int i = 0; i < t.length; i++) {
            if (t[i] != null) {
                if (t[i].isAlive()) {
                    return true;
                }
            }
        }
        return false;
    }
}

